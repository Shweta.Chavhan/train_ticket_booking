package com.telaverge.rabbitmq.publisher;
import java.util.UUID;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telaverge.rabbitmq.dto.Ticket;
import com.telaverge.rabbitmq.dto.BookingStatus;
import com.telaverge.rabbitmq.config.BookingConfig;
@RestController   
//To use as rest endpoint
//@RequestMapping("/home")
@EnableRabbit
public class Sender 
{
	@Autowired
	private  RabbitTemplate template;
//	@PostMapping("/book")
	public String bookTicket(@RequestBody Ticket ticket, @PathVariable String stationname)
	{
		ticket.setTicketId(UUID.randomUUID().toString());
		BookingStatus bookingStatus = new BookingStatus(ticket,"Booked");
	    template.convertAndSend(BookingConfig.EXCHANGE,BookingConfig.ROUTING_KEY, bookingStatus); 
 	    return "Ticket booked ...";	
	}
//	TODO: Learn about RabbitTemplate 
}