package com.telaverge.rabbitmq.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
public class BookingStatus 
{
	public Ticket ticket;
	public String Status;  //shipped, out for deleivery , delivered
	
	public BookingStatus()
	{
		
	}
	public BookingStatus(Ticket ticket, String status)
	{
       this.ticket=ticket; 
       this.Status=status;       
	}

}
