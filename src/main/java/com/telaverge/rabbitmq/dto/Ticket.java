package com.telaverge.rabbitmq.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Ticket 
{
	public String bookingId;
	public String pname; 
	public String trainName;
	//public Date date;
    public String pclass;
    
    public void setTicketId(String id)
    {
    	this.bookingId=id;
    }
}

