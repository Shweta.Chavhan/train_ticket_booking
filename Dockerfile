FROM openjdk:11 as rabbitmq
EXPOSE 9089
WORKDIR /app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY ./src ./src
COPY ./pom.xml ./pom.xml
ENV SPRING_RABBITMQ_HOST hello-world 
RUN chmod 755 /app/mvnw
RUN ./mvnw package -DskipTests
ENTRYPOINT ["java","-jar","target/rabbitmq-0.0.1-SNAPSHOT.jar"] 
